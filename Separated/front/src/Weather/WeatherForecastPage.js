import React from 'react';
import useWeatherForecast from './useWeatherForecast';

function WeatherForecastPage(props) {
    const data = useWeatherForecast();

    return <div>
        <table border="1">
            <caption>Weather Forecast</caption>
            <tr>
                <th>Date</th>
                <th>TemperatureC</th>
                <th>TemperatureF</th>
                <th>Summary</th>
            </tr>
            {data.map((w, index) => <tr id={index}>
                <td>{w.date}</td>
                <td>{w.temperatureC}</td>
                <td>{w.temperatureF}</td>
                <td>{w.summary}</td>
            </tr>)}
        </table>
    </div>
}

export default WeatherForecastPage;