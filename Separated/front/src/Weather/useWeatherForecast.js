import { useState, useEffect } from 'react';

function useWeatherForecast(props) {
    const [weatherData, setWeatherData] = useState([]);

    useEffect(() => {
        fetch("/api/WeatherForecast", {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => response.json())
        .then(data => setWeatherData(data))
        .catch(error => console.log(error))
    }, [])

    return weatherData;
}

export default useWeatherForecast;