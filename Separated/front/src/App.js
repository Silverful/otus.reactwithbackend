import './App.css';
import WeatherForecastPage from './Weather/WeatherForecastPage';

function App() {
  return (
    <div className="App">
      <WeatherForecastPage />
    </div>
  );
}

export default App;
